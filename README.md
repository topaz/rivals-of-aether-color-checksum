This is a small tool to calculate checksums for Rivals of Aether custom color palettes.

It supports reading from stdin or from a file; you can paste in your color code (press ^D when done) or pass a file on the command line.

The checksum byte on the end is optional. If it's included, it'll be checked against; if it's left out, it'll be calculated for you (good for editing the colors).

Dashes are optional too, though they'll be added in the output.

It also supports an ini-style format for checking a bunch of codes at once, e.g.:
```
[Sylvanos]
Frost = 5050-6060-6080-2020-3040-FFA0-40FF-FFC2
Ylfingr = 2020-4040-40A0-4000-80A0-00FF-C080-FF8B
Topaz = F0E0-C0E0-A060-FFF0-D0FF-D000-FFFF-FFE3
```

...though this is mostly irrelevant and it parses anything that looks like a code and ignores anything that doesn't.
