#!/usr/bin/env perl
#
# rivals-of-aether-color-checksum.pl
# by Topaz and Frost
# https://git.brightfur.net/topaz/rivals-of-aether-color-checksum
#
# Usage:
# echo xxxx-xxxx-xxxx-xxxx-xxxx-xxxx-xxxx-xxxx | rivals-of-aether-color-checksum.pl
# cat custom-colors.txt | rivals-of-aether-color-checksum.pl
# rivals-of-aether-color-checksum.pl custom-colors.txt
#
# Public domain -- see COPYING.md for more information.
#

use strict;
use warnings;
use v5.14;

my $file = $ARGV[0];
my $fh = *STDIN;
if ($file) {
	open $fh, "<", $file or die "Can't open file $file: $!\n";
}

while (<$fh>) {
	if (/^\[(.*)\]$/) {
		say "[$1]";
		next;
	}
	my $line = $_;
	my ($name) = /^(.*?) ?[=:] ?/;
	my $nodashes = s/-//gr;
	if ($nodashes =~ /((?:[a-fA-F0-9]{6})+)([a-fA-F0-9]{2})?/) {
		my $colorcode = $1;
		my $checksum_provided = $2;
		# say "$colorcode";
		
		my @colors = grep {/.+/} (split /(.{6})/, $colorcode);
		# say join("/", @colors);
		if (defined $checksum_provided) {
			$checksum_provided = hex($checksum_provided);
			# printf "Provided checksum: %02X\n", $checksum_provided;
		} else {
			# say "No checksum provided";
		}
		
		my $checksum = 0;
		for (my $i = 0; $i <= $#colors; $i++) {
			my $color = $colors[$i];
			my @rgb = ($color =~ /(..)(..)(..)/);
			@rgb = map { hex($_) } @rgb;
			# say "rgb: ", join("/", @rgb);
			
			# calculate the checksum
			my $mask = 0b01100101;
			for (my $j = 0; $j <= $#rgb; $j++) {
				my $component = $rgb[$j];
				# printf "\t%d: %3d: %08b\n", $j, $component, $component;
				for (my $bitpos = 0; $bitpos < 8; $bitpos++) {
					my $bit = ($component & (1 << $bitpos)) != 0;
					if (!$bit) {
						next; # don't add the mask for this byte
					}
					# printf "\t\tbit %d: %b\n", $bitpos, $bit;
					my $check = $mask + $i + $j;
					# printf "\t\tmask: %08b\n", $check;
					$check <<= $bitpos;
					$check %= 0x100;
					# printf "\t\t<< %d = %08b\n", $bitpos, $check;
					$checksum += $check;
				}
			}
		}
		$checksum %= 0x100;
		# printf "FINAL CHECKSUM: %08b = %02X", $checksum, $checksum;
		
		# print it!
		if (defined $name) {
			printf "$name: ";
		}
		my $code = sprintf "%s%02X", $colorcode, $checksum;
		if (length($code) % 4 != 0) {
			# pad to full 4 chars
			$code .= "00";
		}
		$code = join("-", grep {/.+/} split("(....)", $code));
		print $code;
		
		if (defined $checksum_provided) {
			if ($checksum == $checksum_provided) {
				printf " [\e[1;92mOK\e[0m]\n";
			} else {
				printf " [\e[1;91mFAILED\e[0m: %02X != %02X]\n", $checksum, $checksum_provided;
			}
		} else {
			print "\n";
		}
	}
}
